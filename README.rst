###############
Curriculum Vitæ
###############

Consultez la version en ligne à l'adresse https://cblegare.gitlab.io/curriculumvitae/

Service
=======

**Solliciter mes services**
    Contactez-moi au charles.bouchard-legare@cimeconseils.ca

**Demander un CV**
    avec des modifications particulières, `créez un ticket`_.

**Signaler une coquille**
    `créez un ticket`_.


Construire le CV
================

Construisez le conteneur

.. code-block::

    podman build . -t cvbuilder

Utilisez le conteneur

.. code-block::

    podman run --rm -v .:/docs localhost/cvbuilder:latest pip install nox; nox
