Charles Bouchard-Légaré
=======================

> <charlesbouchardlegare@gmail.com> 
> • 
> [+1 (581) 998 6899](tel:+15819986899)

---

> Généraliste passionné
> avec une expérience démontrée en 
> architecture de systèmes distribués à grande échelle, 
> amélioration de processus,
> conception de plateformes,
> traitement de données massives,
> ainsi qu'en sécurité et conformité des technologies.

---

Survol des compétences
----------------------

Cumule 13 années d'expérience en technologies de l'information dont
9 à titre de conseiller en architecture.

- 9 ans en conception de système d'intégration et déploiement continus
  avec GitLab, GitHub Actions, Jenkins et Azure DevOps;
- 7 ans en conception de systèmes infonuagiques avec Google Cloud et Amazon Web Services;
- 4 ans en conception de système événementiels distribués et de traitement
  de données massives avec RabbitMQ, Google Cloud Pub/Sub, Amazon SQS et Kafka;
- 6 ans en conception de services REST avec NodeJS, PHP, Python, C# .Net et Java;
- 10 ans en système de gestion de bases de données incluant 
  2 ans avec SQL Server, 5 ans avec PostgreSQL, 7 ans avec MySQL et
  3 ans avec MongoDB;
- 5 ans en architecture conteneurisée avec Kubernetes, Istio et les outils
  opérationnels associés comme Prometheus, Kibana et LogStash;
- 4 ans en sécurité de l'information;
- une solide expertise en amélioration de processus de développement et automatisation
  d'intégration et de déploiement;
- une expérience démontrée en architecture intégrée avec des systèmes patrimoniaux;
- une sensibilité particulière aux considérations d'assurance 
  qualité et d'exploitation des composantes développées;
- une variété d'expériences dans une multitude de domaines tels que 
  la télécommunication, la traçabilité, les jeux vidéo et le cinéma, 
  l'alimentation, les assurances et les services financiers;
- et se démarque par sa capacité à négocier avec des collaborateurs de toutes 
  les disciplines et par son doigté en gestion de changement.


Environnement technologique
---------------------------

Systèmes d'exploitation
:   * GNU/Linux
    * Mac OS X
    * Windows
    * Autres UNIX (FreeBSD)

Bases de données
:   * PostgreSQL
    * MongoDB
    * MySQL
    * Microsoft SQL Server
    * Redis

Languages de programmation
:   * Python
    * C#
    * Javascript/Typescript
    * PHP
    * HCL (Terraform)
    * BaSH
    * C/C++
    * Perl
    * Java

Environnements de développement
:   * JetBrain (multiples)
    * Vim et terminal
    * VS Code
    * Visual Studio

Infonuagique
:   * Google Cloud Platform
    * Amazon Web Services
    * Microsoft Azure
    * Installations privées

Outils
:   * Git, Subversion, Perforce, Visual SourceSafe
    * Microsoft Office, Google Worksplace
    * Suite Atlassian, Azure DevOps, GitLab, GitHub
    * VMWare, Oracle VirtualBox, KVM, Vagrant


Expériences professionnelles
----------------------------

Architecte infonuagique
:   2023 - 2024;
**Ministère de la cybersécurité et du numérique (en mandat)**

    > *Responsable du produit, Conseiller technique, formateur et leader en soutien au
    > responsable d'architecture.*

    * Participer aux ateliers d'architecture d'une plateforme pour
      développeurs.
    * Mettre en place des pratiques de développements d'infrastructure
      respectant les principes *DevOps* et *Zéro Confiance*.
    * Établir la structure documentaire de la plateforme.

Architecte de programme 
:   2020 - 2023;
**[EXFO](http://www.exfo.com)**

    > *Leader technique et agent de changement de la première unité d'affaire
    > ciblée par une transformation numérique générale.*

    **Tâches:**

    * Superviser la réalisation des travaux réalisés à travers le monde;
    * influencer significativement la culture d'ingénierie
      de l'entreprise en mettant la réalité des utilisateurs au coeur de la conception;
    * soutenir et mentorer les architectes, développeurs, administrateurs de
      systèmes, chargés de produits et spécialistes en assurance qualité;
    * renforcir la culture DevOps du groupe;
    * encadrer les ressources externes participant à la réalisation des produits;
    * renforcir les équipes avec de l'accompagnement personnalisé et en 
      améliorant le recrutement;
    * établir la structure documentaire du programme; des schémas 
      d'infrastructure aux processus créatifs, en passant par l'embauche
      et les éléments de reconnaissance de coûts;
    * assurer les communications pour une collaboration efficace entre 
      l'équipe de développement et les différentes parties prenantes;
    * diriger la réalisation des travaux d'architecture.

    **Projets:**

    * Amélioration des processus de développement et d'intégration.
      * Augmentation observée des fréquences de livraisons d'un facteur 
        de plus de 100, en plus d'augmenter la productivité 
        et la qualité des livrables.

    * Refonte des systèmes d'authentification et authorization.
      * Diminution observée des latences réseau de 95%.

    * Mise à l'échelle des équipes de développement Web.
      * Doubler le nombre d'escouades de développement sans ajouter de
        ressources de coordination.

    * Migration des charges de travail vers Kubernetes.
      * Suppression de centaines de fichiers de configuration d'infrastructure;
      * et escouades de développement mieux focalisées.

    * Investigation pour la portabilités des solutions de réfléctométrie.
      * Définition d'un plan d'action pour mitiger significativement une
        refonte estimée à 135 années-personnes.

    **Environnement technologique:**

    Linux (Debian), C#, Python, Javascript/Typescript, Angular, Dart (Flutter),
    Google Cloud Platform, Amazon Web Services, MongoDB, PostgreSQL, OAuth2,
    Kubernetes, Kafka, Bash, Vim, Git, GitLab, Azure DevOps, Agile, Lean Six Sigma.


Superviseur, Chef architecte, Architecte d'entreprise
:   2017 - 2020;
**[OPTEL](https://www.optelgroup.com/en/)**

    > *Coach et formateur inspirant, architecte de plateforme novateur
    > et gestionnaire soucieux.*

    **Tâches:**

    * Superviser la réalisation des travaux de livraison;
    * soutenir et mentorer les architectes, développeurs, administrateurs de
      systèmes et scientifiques des données;
    * représenter l'organisation devant les audits de sécurité menés par 
      les clients;
    * assurer la supervision et la direction des employés conformément 
      aux politiques et procédures de l'organisation;
    * agir comme responsable de la protection des données relativement au
      *Règlement Général sur la Protection des Données* européen;
    * travailler avec le personnel des ressources humaines pour recruter, 
      interviewer, sélectionner, embaucher et employer un nombre approprié d'employés;
    * satisfaire les exigences et régulations en terme de confidentialité et
      protection des données des clients et des utilisateurs;
    * et diriger la réalisation des travaux d'architecture.

    **Projets:**

    * Fondation d'une nouvelle unité d'affaire basée sur la tracabilité des chaînes 
      d'approvisionnement par un système de traitement massif dans le nuage.
      * Concevoir une plateforme devant recevoir des milliards d'entrées 
        mensuelles et destinée à des  industries associées à la sécurité 
        nationale.

    * Concevoir une plateforme de transformation, aggrégation et valorisation
      de données internes pour un système d'informatique décisionnelle (*BI*).

    * Création d'une plateforme événementielle d'intégration entre les systèmes 
      financiers et manufacturiers internes.
      * Opportunité de repousser de plusieurs années le changement complet
        du progiciel de gestion intégré (ERP).

    * Amélioration des processus de développement logiciel internes.
      * Diminution observée de 89% des temps de livraisons.

    * Mise en place d'un programme de formation global en sécurité de l'information.

    * Refonte du système de stockage et de journalisation des configurations des solutions d'inspection.
      * Diminution observée d'un facteur de 150 des délais de transfer entre les
        usines de Québec, d'Europe, d'Amérique du Sud et d'Asie.

    **Environnement technologique:**

    Linux, Windows 10, C#, ASP .NET, Python, Microsoft SQL Server,
    Google Cloud Platform, Microsoft Azure, Spark, Kubernetes, Nginx, RabbitMQ, 
    Bash, Vim, Git, GitLab, GitHub, Agile, Lean Six Sigma.


Chef d'équipe, Conseiller en architecture DevOps
:   2015 - 2017;
**[Frima](https://www.frimastudio.com/en/)** 

    > *Fournisseur dévoué d'outils pour 250 artisants de jeux
    > vidéos et d'effets spéciaux, utilisant 12 plateformes cibles et de 
    > multiples moteurs.*

    **Tâches:**

    * Proposer des solutions aux besoins d'infrastructures et d'outils
      des équipes de livraison;
    * développer les outils internes utilisés en production de films et 
      de jeux vidéo;
    * gérer la configuration et le bon fonctionnement systèmes d'exploitation,
      engins de rendu et trousseaux de compilation;
    * assurer la fiabilité de services essentiels aux opérations de 
      l'entreprise;
    * surveiller le système quotidiennement et répondre immédiatement 
      aux préoccupations des producteurs et chargés de produits;
    * et soutenir les clients d'ici et d'ailleurs, nottament dans la gestion
      de leurs ressources cryptographiques sensibles.

    **Projets:**

    * Fédération des infrastructures d'hébergement de ressources multimédia.
      * Un total de 3 téraoctets de resources multimédia unifiées sans la 
        moindre minute d'interruption de service.

    * Mise en place d'un «parc de compilation».
      * Recyclage de 50 ordinateurs (Mac, Windows, Linux) dans un parc 
        de compilation automatiquement configuré.

    * Optimisation des temps de compilation.
      * Amélioration observée de 25 heures à moins de 45 minutes.

    * Unification des outils de compilation dans un intergiciel commun.
      * Diminution estimée des coûts de développement d'outils d'intégration de 80%.

    **Environnement technologique:**

    Linux (Debian), Windows 7, Windows 10, C#, C++, Python, Java, 
    Batch, Powershell, Bash, Vim, Jenkins, JFrog Artifactory, Git, Subversion, 
    Perforce, Unity 3D, Unreal, Xcode, Android, iOS, GitLab, Agile, Active Directory,
    Docker, Nginx, Apache, Visual Studio.


Programmeur analyste
:   2013 - 2015
**Ideal Technology**

    > *Programmeur enthousiaste dans un écosystème riche allant de réseaux 
    > sociaux sur mesure jusqu'à l'achat en ligne en passant par 
    > l'automatisation de campagnes de marketing.*

    **Tâches:**

    * Développer l'API web centrale;
    * rédiger la documentation technique;
    * réaliser l'analyse fonctionnelle de l'API;
    * réaliser l'analyste fonctionnelle des besoins de sécurité de l'application;
    * effectuer le découpage organique de l'API utilisant un modèle d'architecure orientée services (SOA);
    * fournir un support constant aux programmeurs utilisateurs de l'API.
    * dffectuer le découpage organique des composantes de sécurité;
    * assurer la rétro-compatibilité avec les fonctions préexistantes;
    * modéliser les données des utilisateurs;
    * programmer les nouvelles composantes de sécurité;
    * dormer et accompagner les programmeurs juniors;
    * garantir la sécurité de l'application par des tests automatisés;
    * et implémenter un cadre de travail Agile.

    **Projets:**

    * Réusinage des processus d'authentification.
      * Refonte de la modélisation des utilisateurs et des processus 
        d'authentification.

    * Refonte de l'API web centralisée.
      * Interface d'authentification centralisée (OAuth2);
      * traitement d'images sur mesure des produits Ideal Protein;
      * et géolocalisation des centres affiliés du monde entier.

    **Environnement technologique:**

    Linux (Debian), Mac OS X, PHP, Javascript, PostgreSQL, Symfony, PHUnit, 
    Vagrant, VirtualBox, Git, Bash, Json, Vim, TDD, OAuth2, Varnish, Nginx, 
    Agile.

Programmeur analyste
:   2012 - 2013;
**Précursoft**

    > *Programmeur de logiciels de gestion et d'outils de vente de voiture,
    > d'assurance et de services financiers.*

    **Tâches:**
    
    * Développer l'application de vente de voitures Maestro F&I;
    * programmer différents rapports complexes avec l'API .NET de Crystal Reports;
    * automatiser le déploiement de bases de données du domaine de l'automobile;
    * assurer la rétrocompatibilité avec l'application patrimoniale héritée;
    * rédiger des requêtes SQL performantes;
    * et participer aux cérémonies Agiles (Scrum).

    **Projets:**    

    * Automatisation des déploiements sur les installations privées de concessionnaires.

    **Environnement technologique:**

    Windows 7, Visual Studio 2010, C# .NET, Microsoft SQL Server, MySQL, 
    Cristal Reports, C++ Borland 5, C++ Borland 6, Microsoft SourceSafe, 
    XML, Agile.


Analyste en sécurité TI
:   2011 - 2012;
**[Revenu Québec](https://www.revenuquebec.ca/en/)**

    > *Stagiaire puis analyste débrouillard impliqué dans la prévention et
    > la défense active des réseaux de Revenu Québec.*

    **Tâches:**

    * Administrer des serveurs de surveillance réseau (NIDS);
    * mettre à l'essai différentes solutions logicielles de surveillance réseau;
    * analyser les d'alertes réseau;
    * établir une stratégie d'inspection sans contact du parc informatique;
    * intervenir en cas de tentative d'intrusion réseau;
    * prévenir les tentatives d'intrusion réseau;
    * rédiger des rapports d'enquête;
    * accomplir des audits de sécurité informatique.
    * développer une interface Web;
    * déployer l'application sur des serveurs à haut niveau de sécurité;
    * former et fournir du support technique aux utilisateurs de l'application.

    **Projets:**

    * Surveillance et détection réseau.
      * Appuyer Revenu Québec dans ces démarches pour assurer la 
        disponibilité de tous ses services informatiques ainsi que la 
        confidentialité et l’intégrité des données confidentielles qu'elle 
        détient.

    * Interface web de surveillance réseau.
      * Programmer une interface web simple mais modulaire permettant 
        aux différents intervenants impliqués dans les enquêtes réseaux 
        d'accéder à l'historique des alertes et d'ajouter dynamiquement 
        des outils à l'interface. 

    **Environnement technologique:**

    Sourcefire Snort NIDS, TELUS Managed IPS, MySQL, Perl, IIS, Apache, 
    Linux (Debian), Linux (Arch Linux), FreeBSD, Windows XP, Visual Basic,
    Ruby, Bash, PHP, Perl, Apache, Visual Basic, VMWare ESXi.


Bénévole, vice-président, animateur, Chef
:   2003-2016
**Les Amusements Lacrypte inc.**

    > *Leader inspirant d'une bande d'artistes bénévoles fantastiques.*

    **Tâches:**

    * Animer des activités thématiques pour jeunes et moins jeunes en plein-air;
    * coordonner tous les responsables et bénévoles lors des activités;
    * assister les scénaristes dans l'élaboration de trames narratives captivantes;
    * présider le comité exécutif en l'absence du président;
    * et diriger la cuisine lors de banquets de plus de 50 personnes, 
      parfois sans eau courante ni électricité.


Formation et langues
--------------------

Formation
:   **BSc, Mathématiques et infomatique** (2014); [Université Laval](https://www.ulaval.ca)

:   **Lean Six Sigma Green Belt (ICGB)** (2019); [COFOMO](https://www.cofomo.com/fr)

:   **IT Information Library Foundations Certification (ITIL)** (2017); [AXELOS](https://www.axelos.com/)

Langues
:   **Français** — Langue maternelle

:   **Anglais** — Bilingue (TOEIC 970/974, 2013)

----

> <charlesbouchardlegare@gmail.com> • 
> [+1 (581) 998 6899](tel:+15819986899) • 
> [\fa{linkedin}](https://www.linkedin.com/in/charlesbouchardlegare/) • 
> [\fa{github}](https://github.com/cblegare)
> \
> 581 avenue Murray #1 - Québec, Canada - G1S4T6
