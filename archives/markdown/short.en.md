Charles Bouchard-Légaré
=======================

---

> IT generalist and open source enthousiast 
> with proven experience with developer enablement through tools, 
> SDKs and coaching, 
> API design and development,
> and IT security and compliance.

---


Experience
----------

today
:   **Program Architect**; 
[EXFO](http://www.exfo.com) (Québec City)

    > *Technical leader for a group of 40 people in 6 development squads
    > building REST APIs and web, mobile, desktop and embedded applications.*

    * Implemented new API guidelines (inspired by [Google Cloud](https://cloud.google.com/apis/design))
      leading to 95% reduced average latency for authorization.

    * Coached teams to rework their development workflow. The group went
      from one deployment every 3 months to about 10 deployments per day.

    * Developed a (*innersource*) reusable tool for internal documentation 
      authoring (as code) and publication, leading to 600% more authors.

2017 - 2020
:   **Manager, Lead Architect, acting Data protection officer**; 
[OPTEL](https://www.optelgroup.com/en/) (Québec City)

    > *Inspired coach and trainer, innovative platform architect and 
    > supportive manager.*

    * Designed a traceability platform and managed the cross-functional 
      team that developed it while the pandemic hits and layoffs were announced.
    * Trained all employees across the globe on information security, leading
      to some people so inspired they changed their career path.
    * Coached the internal IT development team to rework their development 
      workflow, leading to 89% reduced lead time to delivery.
    * Initiated the adoption of Linux as an officially supported workstation
      OS, along with a lively related community of practice.

2015 - 2017
:   **Team lead, DevOps achitect, Build master**;
[Frima](https://www.frimastudio.com/en/) (Québec City) 

    > *Devoted CI toolchain provider for 250 craftpeople supporting
    > 12 target platforms and using several engines in a game and FX 
    > studio.*

    * Unified 6 Apache Subversion servers in one, totaling 3 TB of
      files, no downtime.
    * Optimized game compilation time from 25 hours down to 45 minutes.
    * Maintained critical development services
      such as Gitlab, Artifactory, Jenkin, SVN.

2013 - 2015
:   **Programmer**;
Ideal Technology (Québec City)

    * Implemented exotic authorization patterns and provided SDKs to client
      application developers.

2012 - 2013
:   **Programmer**;
Précursoft (Québec City)

    * Automated deployments, namely migrations of automotive listing databases.
    * Fixed years-old SQL bug responsible for loss of hundreds of thousands dollars.

2011 - 2012
:   **IT security analyst**;
[Revenu Québec](https://www.revenuquebec.ca/en/) (Québec City)

    * Set up open source NIDS with related tools. Triaged 8 million
      daily security alerts.

Technical Experience
--------------------

Open source
:   Particularly interested in improving the 
    [Sphinx doc](https://www.sphinx-doc.org/en/master/) ecosystem lately.

    * [Sphinx-Terraform](https://cblegare.gitlab.io/sphinx-terraform/) — 
      A Sphinx extension for documenting Terraform modules.
    * [Sphinx-Gherkin](https://cblegare.gitlab.io/sphinx-gherkin/) —
      A Sphinx extension for documenting Gherkin features.
    * [Sphinx-Compendia](https://cblegare.gitlab.io/sphinx-compendia/) —
      A simple API for creating Sphinx domains and structuring arbitrary 
      collections.

Social
:   Hobbyist contributor on a few platforms (see links).

    [Github](https://github.com/cblegare), 
    [Stack Exchange](https://stackexchange.com/users/2890036/abstrus?tab=reputation),
    [Gitlab](https://gitlab.com/cblegare), 
    [Reddit](https://www.reddit.com/user/cblegare/comments/?sort=hot)
    (r/Python comments are most relevant),
    [Early career blog](http://cblegare.github.io/)
    (8 years old, not much, but a honest attempt!)

Skills
:   **Polynomial scale of experience**

:   **10K+ hours**: Python, Shell scripting, Git, Technical Writing (RestructuredText)
    Linux, Gitlab, Github, LaTeX, Google Cloud Platform, Google Workplace

:   **1K+ hours**: PHP, C#, C++, Javascript/Typescript, MySQL, PostgreSQL,
    Qt, Mac, Windows, Apache Subversion, JFrog Artifactory, Kubernetes,
    Amazon Web Services

:   **100+ hours**: Java, Groovy, C, Perl, OCaml, Perforce, Azure


Education
---------

2009-2014
:   **BSc, Mathematics and Computer Science**; [Université Laval](https://www.ulaval.ca/en) 
    (Québec City)

2019
:   **Lean Six Sigma Green Belt (ICGB)** [COFOMO](https://www.cofomo.com/fr)

2017
:   **IT Information Library Foundations Certification (ITIL)** [AXELOS](https://www.axelos.com/)


More about me
-------------

Languages
:   **Excellent written and verbal communication skills**

:   **French** — Native speaker

:   **English** — Fluent (TOEIC 970/974, 2013)

:   **Spanish** — Conversational

:   **German** — Basic


Interests
:   Free software, hiking, mycology, Wing chun kung fu, psychology

----

> <charlesbouchardlegare@gmail.com> • 
> [+1 (581) 998 6899](tel:+15819986899) • 
> [\fa{linkedin}](https://www.linkedin.com/in/charlesbouchardlegare/) • 
> [\fa{github}](https://github.com/cblegare)
> \
> 1012, rue des Ébénistes - Québec, Canada
