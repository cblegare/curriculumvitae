Charles Bouchard-Légaré
=======================

> <charlesbouchardlegare@gmail.com> 
> • 
> [+1 (581) 998 6899](tel:+15819986899)

---

> Généraliste passionné
> avec une expérience démontrée en 
> architecture de systèmes distribués à grande échelle, 
> amélioration de processus,
> conception de plateformes,
> traitement données massives
> ainsi qu'en sécurité et conformité des technologies.

---


Expérience
----------

2023 - aujourd'hui
:   **Conseiller en architecture infonuagique**; 
Ministère de la cybersécurité et du numérique (en mandat) (Ville de Québec)

    > *Conseiller technique, formateur et leader en soutien au
    > responsable d'architecture.*

    * Participer aux ateliers d'architecture d'une plateforme pour
      développeurs.
    * Mettre en place des pratiques de développement d'infrastructure
      respectant les principes *DevOps* et *Zéro Confiance*.
    * Établir la structure documentaire de la plateforme.


2020 - 2023
:   **Architecte de programme**; 
[EXFO](http://www.exfo.com) (Ville de Québec)

    > *Leader technique et agent de changement de la première unité d'affaire
    > ciblée par une transformation numérique générale.*

    * Superviser la réalisation des travaux réalisés à travers le monde.
    * Revoir l'architecture, réduisant les latences réseaux de 95%.
    * Renforcir la culture DevOps pour livrer plus de 100 fois plus fréquement.
    * Encadrer les ressources externes participant à la réalisation des produits.s
    
[comment]: #    * Superviser la réalisation des travaux réalisés à travers le monde.
[comment]: #    * Influencer significativement la culture d'ingénierie
[comment]: #      de l'entreprise en mettant la réalité des utilisateurs au coeur de la conception.
[comment]: #    * Concevoir les standards d'architecture réduisant les latences réseaux
[comment]: #      de 95%.
[comment]: #    * Renforcir la culture DevOps pour livrer plus de 100 fois plus souvent.
[comment]: #    * Établir la structure documentaire du programme; des schémas 
[comment]: #      d'infrastructure aux processus créatifs, en passant par l'embauche
[comment]: #      et les éléments de reconnaissance de coûts.
[comment]: #    * Encadrer les resources externes participant à la réalisation des produits.
[comment]: #    * Renforcir les équipes avec de l'accompagnement personnalisé et en 
[comment]: #      améliorant le recrutement. 



2017 - 2020
:   **Superviseur, Chef architecte, Architecte d'entreprise**; 
[OPTEL](https://www.optelgroup.com/en/) (Ville de Québec)

    > *Coach et formateur inspirant, architecte de plateforme novateur
    > et gestionnaire soucieux.*

    * Concevoir une plateforme de traçabilité des chaînes d'approvisionnement
      devant recevoir des milliards d'entrées mensuelles.
    * Former tous les employés à travers le monde en sécurité de l'information.
    * Satisfaire les exigences et régulations en terme de confidentialité et
      protection des données des clients et des utilisateurs.
    * Transformer le stockage des configurations pour diminuer
      de 150 fois les délais de transfer entre les usines de Québec et l'Inde.
    * Réformer les processus de développement logiciel internes pour réduire
      de 89% le temps de livraison.


2015 - 2017
:   **Chef d'équipe, Conseiller en architecture DevOps**;
[Frima](https://www.frimastudio.com/en/) (Ville de Québec) 

    > *Fournisseur dévoué d'outils pour 250 artisants de jeux
    > vidéos et d'effets spéciaux, utilisant 12 plateformes cibles et de 
    > multiples moteurs.*

    * Unifier les infrastructures d'hébergement de ressources multimédia
      totalisant 3 téraoctets sans la moindre minute d'interruption de service.
    * Optimiser le temps de compilation de logiciels pour passer de 25 heures
      à moins de 45 minutes.
    * Assurer la fiabilité de services essentiels aux opérations de l'entreprise.
    * Soutenir les clients d'ici et d'ailleurs.


2013 - 2015
:   **Programmeur analyste**;
Ideal Technology (Ville de Québec)

    * Programmer les serveurs Web et implémenter des processus de 
      sécurité exotiques.
    * Soutenir les développeurs d'applications clientes.


2012 - 2013
:   **Programmeur analyste**;
Précursoft (Ville de Québec)

    * Automatiser les déploiements, notamment la migration de base de 
      données automobiles. Déceler et corriger un bogue
      responsable de centaines de milliers de dollars en pertes.


2011 - 2012
:   **Analyste en sécurité TI**;
[Revenu Québec](https://www.revenuquebec.ca/en/) (Ville de Québec)

    * Mettre en place un système de détection d'intrusions. Trier 
      8 millions d'alertes de sécurité quoditiennes.


[comment]: # Expériences complémentaires
[comment]: # ---------------------------
[comment]: # 
[comment]: # 2009 - aujourd'hui
[comment]: # :   **Contributeur volontaire**
[comment]: # sur diverses plateformes en ligne
[comment]: # 
[comment]: #     * Répondre à des questions, corriger des défauts, écrire des tutoriels,
[comment]: #       des articles et autres documentations sur différentes plateformes
[comment]: #       communautaires de praticiens techniques.
[comment]: # 
[comment]: # 2003 - 2016
[comment]: # :   **Bénévole, vice-président, animateur, Chef**;
[comment]: # Les Amusements Lacrypte inc. (Ville de Québec)
[comment]: # 
[comment]: #     * Animer des activités thématiques pour jeunes et moins jeunes en plein-air.
[comment]: #     * Coordonner tous les responsables et bénévoles lors des activités.
[comment]: #     * Présider le comité exécutif en l'absence du président.
[comment]: #     * Diriger la cuisine lors de banquets de plus de 50 personnes, parfois sans eau courante ni électricité.


Formation et compétences
------------------------

Formation
:   **BSc, Mathématiques et infomatique** (2014); [Université Laval](https://www.ulaval.ca) 
    (Ville de Québec)

:   **Lean Six Sigma Green Belt (ICGB)** (2019); [COFOMO](https://www.cofomo.com/fr)

:   **IT Information Library Foundations Certification (ITIL)** (2017); [AXELOS](https://www.axelos.com/)

Compétences techniques
:   **10K+ heures** — Google Cloud Platform, Amazon Web Services,
    Linux, Gitlab, Github, Kubernetes, Jenkins, Python, Shell, PHP, 
    Terraform

:   **1K+ heures** — .Net, C++, NodeJS, MySQL, PostgreSQL,
    Mac, Windows, Java, 

:   **100+ heures** — C, Perl, OCaml, Perforce, Azure, Dart

Langues
:   **Français** — Langue maternelle

:   **Anglais** — Bilingue (TOEIC 970/974, 2013)

----

> <charlesbouchardlegare@gmail.com> • 
> [+1 (581) 998 6899](tel:+15819986899) • 
> [\fa{linkedin}](https://www.linkedin.com/in/charlesbouchardlegare/) • 
> [\fa{github}](https://github.com/cblegare)
> \
> 1012, rue des Ébénistes - Québec, Canada - G3K 0M1
