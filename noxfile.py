from __future__ import annotations

import os
import shutil
from pathlib import Path
from typing import List

import nox


repo_root = Path(__file__).parent
build_root = repo_root / "build"
dist_root = repo_root / "dist"
build_root.mkdir(parents=True, exist_ok=True)


@nox.session(reuse_venv=True)
def sphinxgettext(session: nox.Session):
    session.install("-r", "src/sphinxcv/requirements.txt")
    session.run(
        *[
            str(part)
            for part in _sphinx_build_cmd("gettext")
        ]
    )


@nox.session(reuse_venv=True)
def sphinxintl(session: nox.Session):
    session.install("-r", "src/sphinxcv/requirements.txt")
    session.run(
        "sphinx-intl", "--config", "src/sphinxcv/conf.py", "update", "-p", "build/sphinxcv/gettext", "-l", "en"
    )



@nox.session(reuse_venv=True)
def sphinxpdf(session: nox.Session):
    """
    Build CV from src/sphinxcv, french and english.
    """

    session.install("-r", "src/sphinxcv/requirements.txt")

    source_directory = repo_root.joinpath("src/latexcv")
    moderncv_directory = source_directory.joinpath("vendor/github.com-moderncv-moderncv-master").resolve()

    proc_env = os.environ.copy()
    proc_env["TEXINPUTS"] = f"{moderncv_directory}:{proc_env.get('TEXINPUTS', '')}"

    for language in ("fr", "en"):
        proc_env["SPHINX_LANGUAGE"] = language
        session.run(
            *[
                str(part)
                for part in _sphinx_build_cmd("latex", language=language)
            ],
            env=proc_env,
        )

        build_directory = build_root.joinpath(f'sphinxcv/latex/{language}')
        built_file = build_directory.joinpath("cv.pdf")
        dist_file = dist_root.joinpath(f"Charles Bouchard-Légaré - CV - {language}.pdf")

        with session.chdir(build_directory):
            session.run(
                *[
                    "latexmk",
                    "-pdf",
                    "-dvi-",
                    "-ps-",
                    f"-output-directory={build_directory}",
                    "cv.tex",
                ]
            )

            dist_root.mkdir(exist_ok=True, parents=True)
            shutil.copy(built_file, dist_file)


@nox.session(reuse_venv=True)
def latexpdf(session: nox.Session):
    """
    Build CV from src/latexcv, french and english
    """
    source_directory = repo_root.joinpath("src/latexcv")
    moderncv_directory = source_directory.joinpath("vendor/moderncv-master-2024-11-04").resolve()

    for main_source in (
            "cv_long_fr",
            "cv_short_en",
            "cv_court_fr",
            "lettre_fr"
    ):

        main_source_file = source_directory.joinpath(f"{main_source}.tex")
        build_directory = build_root.joinpath("latexcv")
        dist_file = dist_root.joinpath(f"Charles Bouchard-Légaré - {main_source} - CV.pdf")
        built_file = build_directory.joinpath(f"{main_source_file.stem}.pdf")

        proc_env = os.environ.copy()
        proc_env["TEXINPUTS"] = f"{moderncv_directory}:{proc_env.get('TEXINPUTS', '')}"

        with session.chdir(source_directory):
            session.run(
                *[
                    str(part)
                    for part in latex_build_cmd(main_source_file)
                ],
                env=proc_env,
            )

        dist_root.mkdir(exist_ok=True, parents=True)

        shutil.copy(built_file, dist_file)


@nox.session(reuse_venv=True)
def website(session: nox.Session):
    session.install("-r", "src/sphinxcv/requirements.txt")

    source_dir = repo_root.joinpath("src/sphinxcv")

    pdfs = dist_root.glob("*.pdf")

    for pdf in pdfs:
        shutil.copy(pdf, source_dir.joinpath("_static"))

    proc_env = os.environ.copy()

    proc_env["SPHINX_LANGUAGE"] = "fr"
    session.run(
        *[
            str(part)
            for part in _sphinx_build_cmd(language="fr")
        ],
        env=proc_env,
    )

    proc_env["SPHINX_LANGUAGE"] = "en"
    session.run(
        *[
            str(part)
            for part in _sphinx_build_cmd(language="en")
        ],
        env=proc_env,
    )
    shutil.copy(source_dir.joinpath("_redirects"), build_root.joinpath(f"sphinxcv/html/"))



def latex_build_cmd(source_file: str) -> List[str|Path]:
    return [
        "latexmk",
        "-pdf",
        f"-output-directory={build_root.joinpath('latexcv')}",
        source_file,
    ]


def _sphinx_build_cmd(builder: str = "html", language: str = "fr") -> List[str|Path]:
    return [
        "python",
        "-m"
        "sphinx",
        "-v",
        "-b",
        builder,
        "-d",
        build_root.joinpath("doctrees"),
        "-E",
        "-n",
        "-W",
        "--keep-going",
        "-T",
        "-D",
        f"language={language}",
        repo_root.joinpath("src/sphinxcv"),
        build_root.joinpath(f"sphinxcv/{builder}/{language}"),
    ]
