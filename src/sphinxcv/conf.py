# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import os
from datetime import date

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Curriculum Vitæ"
author = "Charles Bouchard-Légaré"
copyright = f"2024, {author}"
release = str(date.today())

language = os.getenv("SPHINX_LANGUAGE", "fr")

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinxcontrib.jquery",
    "sphinx_datatables",
]

master_doc = "index"

templates_path = ["_templates"]
exclude_patterns = []
pygments_style = "lovelace"


# -- I18N
locale_dirs = [
    "_locale/"
]
gettext_compact = False

# -- Extensions

# Datatable
datatables_version = "1.13.4"
datatables_options = {
    "pageLength": 50,
    "language": {
        "url": "_static/dataTables.french.json",
    },
}

# -- Latex configuration

latex_elements = {
    # The paper size ("letterpaper" or "a4paper").
    "papersize": "letterpaper",
    # The font size ("10pt", "11pt" or "12pt").
    "pointsize": "10pt",
    # Additional stuff for the LaTeX preamble.
    "preamble": r"""""",
    # Remove ToC
    "tableofcontents": "",
    "maketitle": r"""\sphinxmaketitle"""
}

# We use Unicode character, so xelatex to the rescue.
latex_engine = "xelatex"

latex_documents = [
    (
        master_doc,
        "{!s}.tex".format("cv", release),
        project,
        author,
        "howto",
        False,
    ),
]
# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_css_files = ["style.css",]
html_show_sourcelink = True
html_show_sphinx = True
html_show_copyright = True

html_theme_options = {
    "icon_links": [
        {
            "name": "Dépôt Git de ce site sur GitLab",
            "url": "https://gitlab.com/cblegare/curriculumvitae",
            "icon": "fab fa-gitlab",
        },
        {
            "name": "cblegare sur GitHub",
            "url": "https://github.com/cblegare",  # required
            "icon": "fa-brands fa-github",
            "type": "fontawesome",
        },
        {
            "name": "Charles sur Linkedin",
            "url": "https://www.linkedin.com/in/charlesbouchardlegare/",  # required
            "icon": "fa-brands fa-linkedin-in",
            "type": "fontawesome",
        },
    ],
    "icon_links_label": "Quick Links",
    "external_links": [
        {
            "name": "Télécharger PDF",
            "url": "./_static/Charles Bouchard-Légaré - long_fr - CV.pdf",
        } if language == "fr" else {
            "name": "Download PDF",
            "url": "./_static/Charles Bouchard-Légaré - short_en - CV.pdf",
        }, {
            "name": "Nous écrire" if language == "fr" else "Contact us",
            "url": "mailto:charlesbouchardlegare@gmail.com",
        },
    ],
    "show_prev_next": True,
    # Version (language) switcher configuration
    "switcher": {
        "json_url": "https://cblegare.gitlab.io/curriculumvitae/fr/_static/switcher.json",
        "version_match": language,
    },
    "check_switcher": False,
    "announcement": "This page was translated automatically and may not be accurate. Manual translation is in progress. See <a href='https://cblegare.gitlab.io/curriculumvitae'>the original french version</a>." if language == "en" else "",
    "navbar_center": ["navbar-nav", "appointmentbutton"],
    "navbar_end": ["navbar-icon-links", "version-switcher"],
    "navigation_with_keys": False,
}

html_sidebars = {
    #"**": [
    #    "search-field",
    #    "sidebar-nav-bs.html",
    #    # "localtoc.html",
    #]
}

# A dictionary of values to pass into the template engine’s context for all
# pages. Single values can also be put in this dictionary using the
# -A command-line option of sphinx-build.
if os.getenv("CI_MERGE_REQUEST_IID", None):
    review_app = {
        "project_id": os.getenv("CI_PROJECT_ID"),
        "project_path": os.getenv("CI_PROJECT_PATH"),
        "mr_id": os.getenv("CI_MERGE_REQUEST_IID"),
        "server_url": os.getenv("CI_SERVER_URL"),
    }
else:
    review_app = None
html_context = {
    # "gitlab_url": "https://gitlab.com", # or your self-hosted GitLab
    "gitlab_user": "cblegare",
    "gitlab_repo": "curriculumvitae",
    "gitlab_version": "main",
    "doc_path": "src/sphinxcv",
    # Enable the Giltab review app widget,
    # see also
    #   _templates/layout.html
    #   https://docs.gitlab.com/ee/ci/review_apps/#configuring-visual-reviews
    "review_app": review_app,
    "rendezvous_btn_label": "Réserver un rendez-vous" if language == "fr" else "Schedule an Appointment",
    "support_btn_label": "Nous écrire" if language == "fr" else "Contact us",
}

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = project
