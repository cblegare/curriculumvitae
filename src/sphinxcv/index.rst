
.. meta::
   :keywords: CV Resume Curriculum Vitæ Charles Bouchard Légaré cblegare CIME
   :description: Curriculum vitæ de Charles Bouchard-Légaré


#######################
Charles Bouchard-Légaré
#######################

.. container:: tagline

    Cumule **13 années** d'expérience en technologies de l'information
    à titre de **conseiller en gestion et en architecture**,
    de **gestionnaire**
    et de **programmeur**
    dans une variété de domaines tels que
    les hautes technologies,
    la sécurité de l'information,
    les jeux vidéo et le cinéma,
    les assurances et les services financiers.
    J'aide les praticiens techniques à se **concentrer sur leur spécialité**.

*   **13 ans** à écrire du code propre, dont 10 en Python
*   **11 ans** d'automatisation des opérations
*   **10 ans** avec des systèmes de base de données
*   **9 ans** à accompagner des équipes de développement, de scientifiques et d'artistes
*   **7 ans** à construire pour le *Cloud* avec GCP et AWS
*   **5 ans** comme coach de gestion Lean et Agile
*   **6 ans** en architecture conteneurisée avec Kubernetes
*   **5 ans** avec des données massives et systèmes distribués
*   **3 ans** en soutient à des projets d'intelligence artificielle


Langues
=======

Français
    Excellent expression orale et écrite

Anglais
    Bilingue (TOEIC: 970/974, 2013)


Formation
=========

Académique
    *   **Baccalauréat ès sciences**, *2009 - 2014*, Université Laval, Mathématiques et informatique

Certifications
    *   **Professional Scrum Master I**, *avril 2024*, Scrum.org
    *   **Lean Six Sigma Green Belt (ICGB)**, *mars 2019*, COFOMO
    *   **IT Information Library Foundations Certification (ITIL)**, *mars 2017*, AXELOS Global Best Practice, diplôme \#04110008-01-AK8G

Cours
    *   **D'employé à gestionnaire : Réussir sa prise de poste**, *avril 2017*, AFI Expertise


Compétences
===========

.. list-table::
    :header-rows: 1
    :class: sphinx-datatable longtable
    :widths: 8 10 15 10 45

    *   *   Type
        *   Niveau
        *   Compétence
        *   Années
        *   Commentaire

    *   *   Code
        *   ●●●●○
        *   HCL (Terraform)
        *   9
        *   Multiples projets de grande à très grande envergure.
    *   *   Code
        *   ●●●●●
        *   Python
        *   10
        *   Des dizaines de projets d'automatisation, service webs, ligne de commande et application de bureau
    *   *   Code
        *   ●●●●○
        *   BaSH
        *   15+
        *   Vaste expérience, incluant sur des système anciens ou exotiques
    *   *   Code
        *   ●●●○○
        *   C\#
        *   4
        *   Bon programmeur, énorme expérience en automatisation et outillage
    *   *   Code
        *   ●●●○○
        *   Javascript/Typescript
        *   7
        *   Bon programmeur, énorme expérience en automatisation et outillage
    *   *   Code
        *   ●●●○○
        *   PHP
        *   3
        *   Utilisé avec Symfony
    *   *   Code
        *   ●●○○○
        *   C/C++
        *   4
        *   Utilisé en début de carrière
    *   *   Code
        *   ●●○○○
        *   Java
        *   2
        *   Peu comme programmeur, grande expérience en automatisation et outillage
    *   *   Code
        *   ●●●○○
        *   LaTeX
        *   5
        *   Ce CV est aussi disponible en PDF depuis LaTeX

    *   *   OS
        *   ●●●●○
        *   Linux
        *   15+
        *   Utilisateur et administrateur Linux depuis 2008
    *   *   OS
        *   ●●●○○
        *   OSX
        *   7
        *   Administrateur OSX pour une ferme de compilation
    *   *   OS
        *   ●●○○○
        *   Windows
        *   15+
        *   Utilisateur Windows depuis 3.1
    *   *   OS
        *   ●○○○○
        *   autres UNIX
        *   2
        *   Quelques expérience avec FreeBSD et autres systèmes spécialisés

    *   *   BD
        *   ●●●●○
        *   PostgreSQL
        *   5+
        *   Sur site, dans AWS et dans GCP
    *   *   BD
        *   ●●●○○
        *   MySQL
        *   5
        *   Utilisateur et administrateur
    *   *   BD
        *   ●●○○○
        *   Microsoft SQL Server
        *   5
        *   Comme programmeur seulement
    *   *   BD
        *   ●●●○○
        *   MongoDB
        *   3
        *   Hébergé par MongoDB Atlas

    *   *   Cloud
        *   ●●●○○
        *   Google
        *   5
        *   Kubernetes, mise en place pour d'autres développeurs
    *   *   Cloud
        *   ●●●○○
        *   Amazon
        *   5
        *   Lambda, EKS et expérience au sein de ASEA dans une organisation gouvernmentale
    *   *   Cloud
        *   ●○○○○
        *   Microsoft
        *   1
        *   Collaborateur d'équipes d'administration Azure
    *   *   Cloud
        *   ●●○○○
        *   Installations privées
        *   4
        *   Expérience au sein d'organisations privées et gouvernementales

    *   *   Outil
        *   ●●●●○
        *   Gestion des sources
        *   15+
        *   Git, Subversion, Perforce, SourceSafe
    *   *   Outil
        *   ●●●○○
        *   Bureautique
        *   15+
        *   Microsoft Office, Google Worksplace
    *   *   Outil
        *   ●●●●●
        *   CI/CD
        *   10+
        *   GitLab, GitHub Action, Azure, Jenkins
    *   *   Outil
        *   ●●●●○
        *   Planification
        *   10+
        *   Azure Boards, Jira, GitLab, GitHub
    *   *   Outil
        *   ●●●○○
        *   Virtualisation
        *   10+
        *   VMWare, Oracle VirtualBox, KVM, Vagrant, Docker, Podman

    *   *   Méthode
        *   ●●●○○
        *   Scrum
        *   10+
        *   Expérience comme PO et SM
    *   *   Méthode
        *   ●●●○○
        *   Lean Six Sigma
        *   5
        *   Plusieurs projets réussis comme Coach
    *   *   Méthode
        *   ●●●○○
        *   SAFe
        *   2
        *   Expérience comme PO
    *   *   Méthode
        *   ●●○○○
        *   Assurance Qualité
        *   3
        *   Participé à l'amélioration de manuels d'assurance qualité
    *   *   Méthode
        *   ●●●●○
        *   TDD
        *   3
        *   Expérience comme formatteur.
    *   *   Méthode
        *   ●●●●●
        *   DevSecOps
        *   9
        *   Expérience comme concepteur, architecte, formatteur, coach, etc.

    *   *   Autres
        *   ●●●○○
        *   Ateliers Lean/Agile
        *   5
        *   Facilitateur d'atelier éprouvé
    *   *   Autres
        *   ●●●●○
        *   Documentation technique
        *   5
        *   Auteur de logiciels libres documentés
    *   *   Autres
        *   ●●●○○
        *   Documentation d'architecture
        *   5
        *   C4, UML, documentation par le code
    *   *   Autres
        *   ●●○○○
        *   Documentation d'affaire
        *   3
        *   BPMN, draw.io, Stories
    *   *   Autres
        *   ●●●○○
        *   Formation
        *   5
        *   Formateur en sécurité de l'information, architecture, développement


Expérience professionnelle
==========================


Conseiller en architecture de solution
---------------------------------------

*2024*, Desjardins

    Gardien de la cohérence fonctionnelle, coach aux analystes.

*   Conseiller et accompagner les partenaires d'affaires et
    spécialistes dans le positionnement, la planification, le
    développement, la réalisation et les suivis relatifs aux solutions
    technologiques.
*   Voir au développement et à l’évolution de politiques, de normes,
    de cibles d’architecture et les fondations qui les soutiennent
    ou de solutions technologiques tenant compte des aspects touchants
    les données, les applications et les infrastructures.
*   Soutenir les équipes d'analyse d'affaires et fonctionnelles

Projets:
    Refonte des feuillets fiscaux

Méthodes:
    Kanban,
    Scrum_

Environnement:
    UML,
    C4


Conseiller en architecture infonuagique
---------------------------------------

*2023 - 2024*, Ministère de la cybersécurité et du numérique

    Responsable du produit, Conseiller technique, formateur et leader en
    soutien au responsable d'architecture.

*   Prioriser les livrables d'infrastructure à réaliser
*   Concevoir, implémenter et maintenir l'infrastructure
*   Participer aux ateliers d'architecture d'une plateforme pour développeurs
*   Mettre en place des pratiques de développements d'infrastructure respectant les principes *DevOps* et *Zéro Confiance*
*   Établir la structure documentaire de la plateforme

Projets:
    Plateforme de développement moderne (*PDM*)

Méthodes:
    SAFe_,
    Scrum_,
    Nexus_,
    IaC_,
    DocAsCode

Environnement:
    Linux (Alma), Python, Amazon Web Services, Terraform
    Kubernetes, Bash, Git, Azure DevOps,
    UML,
    C4


Architecte du programme
-----------------------

*2020 - 2023*, `EXFO <http://www.exfo.com>`__

    Leader technique et agent de changement de la première unité d'affaire
    ciblée par une transformation numérique générale.

*   Superviser la réalisation des travaux à travers le monde
*   Mentorer les architectes, développeurs, administrateurs de systèmes, chargés de produits et spécialistes en assurance qualité
*   Renforcir la culture DevOps du groupe
*   Encadrer les ressources externes participant à la réalisation des produits
*   Soutenir les équipes avec de l'accompagnement personnalisé et en améliorant le recrutement
*   Établir la structure documentaire du programme; des schémas d'infrastructure aux processus créatifs, en passant par l'embauche et les éléments de reconnaissance de coûts
*   Assurer les communications pour une collaboration efficace entre l'équipe de développement et les différentes parties prenantes
*   Diriger la réalisation des travaux d'architecture

Projets:
    *   Amélioration des processus de développement et d'intégration

        *   Augmentation observée des fréquences de livraisons d'un facteur
            de plus de 100, en plus d'augmenter la productivité
            et la qualité des livrables.

    *   Refonte des systèmes d'authentification et authorization

        *   Diminution observée des latences réseau de 95%.

    *   Migration des charges de travail vers Kubernetes

        *   Suppression de centaines de fichiers de configuration d'infrastructure
        *   et escouades de développement mieux focalisées.

    *   Investigation pour la portabilité des solutions de réflectométrie

        *   Définition d'un plan d'action pour mitiger significativement une
            refonte estimée à 135 années-personnes.
        *   Preuves de concept en Python pour permettre
            l'utilisation d'apprentissage automatique en soutien à l'analyse de signal,
            les équipes ML ont réussi à arriver à parité de précision avec
            les algorithmes traditionnels.
        *   Conception d'un système de livraison des modèles
            d'apprentissage machine.

Méthodes:
    `Lean Six Sigma`_
    POO_,
    SOA_,
    REST_,
    TDD_,
    `CI/CD`_,
    SAFe_,
    Scrum_,
    Kanban_,
    Nexus_,
    IaC_,
    DocAsCode

Environnement:
    Linux (Debian), C\#, Python, Javascript/Typescript, Angular, Dart (Flutter),
    Google Cloud Platform, Amazon Web Services, MongoDB, PostgreSQL, OAuth2,
    Kubernetes, Kafka, Bash, Vim, Git, GitLab, Azure DevOps


Architecte en chef
------------------

*2017 - 2020*, `OPTEL <http://www.optelgroup.com>`__

Autres rôles occupés: **superviseur**, **responsable de la sécurité TI**
et **architecte d'entreprise**

    Coach et formateur inspirant, architecte de plateforme novateur et
    gestionnaire concerné.


*   Superviser la réalisation des travaux de livraison
*   Soutenir et mentorer les architectes, développeurs, administrateurs de systèmes et scientifiques des données
*   Représenter l'organisation devant les audits de sécurité menés par les clients
*   Assurer la supervision et la direction des employés conformément aux politiques et procédures de l'organisation
*   Agir comme responsable de la protection des données relativement au *Règlement Général sur la Protection des Données* européen
*   Travailler avec le personnel des ressources humaines pour recruter, interviewer, sélectionner, embaucher et employer un nombre approprié d'employés
*   Satisfaire les exigences et régulations en terme de confidentialité et protection des données des clients et des utilisateurs
*   Diriger la réalisation des travaux d'architecture

Projets:
    *   Mise en place d'une nouvelle unité d'affaire basée sur la traçabilité
        des chaînes d'approvisionnement par un système de traitement massif
        dans le nuage

        *   Concevoir une plateforme devant recevoir des milliards d'entrées
            mensuelles et destinée à des industries associées à la sécurité
            ationale.
        *   Données massives pour informatique décisionnelle (*BI*)
        *   Plusieurs petit projets utilisant des modèles d'apprentissage
            machine, surtout en classification d'images

    *   Création d'une plateforme d'intégration entre les systèmes financiers
        et manufacturiers internes

        *   Opportunité de repousser de plusieurs années le changement
            complet du progiciel de gestion intégré (ERP).

    *   Création d'une plateforme de transformation, aggrégation et
        valorisation de données internes pour un système d'informatique
        décisionnelle (*BI*)

        *   Définir les processus de développement et l'architecture
            technologique.
        *   Après un an, l'équipe garde une vélocité croissante et démontre
            un haut niveau de maturité *DevOps*.

    *   Amélioration les processus de développement logiciel internes

        *   Mise en place d'un système d'amélioration continue (*Kaizen*).
        *   Diminution observée de 89% des temps de livraisons.

    *   Modernisation du système de stockage et de journalisation des
        configurations des solutions d'inspection

        *   Diminution observée d'un facteur de 150 des délais de transfer
            entre les usines de Québec, d'Europe, d'Amérique du Sud et
            d'Asie.

    *   Mise en place d'un programme de formation global en sécurité de
        l'information.

        *   Près de mille (1000) employés formés sur 4 continents.

Méthodes:
    `Lean Six Sigma`_
    POO_,
    SOA_,
    REST_,
    TDD_,
    `CI/CD`_,
    Scrum_,
    Kanban_,
    IaC_,
    DocAsCode

Environnement:
    Linux, Windows 10, C\#, ASP .NET, Python, Microsoft SQL Server,
    Google Cloud Platform, Microsoft Azure, Spark, Kubernetes, Nginx, RabbitMQ,
    Bash, Vim, Git, GitLab, GitHub.



Conseiller en architecture DevOps
---------------------------------

*2015 - 2017*, `Frima <http://www.frimastudio.com>`__

Autres rôles occupés: **Chef d'équipe**

    Fournisseur dévoué d'outils pour 250 artisants de jeux vidéos et d'effets
    spéciaux, utilisant 12 plateformes cibles et de multiples moteurs.

*   Proposer des solutions aux besoins d'infrastructures et d'outils des équipes de livraison
*   Développer les outils internes utilisés en production de films et de jeux vidéo
*   Gérer la configuration et le bon fonctionnement systèmes d'exploitation, engins de rendu et trousseaux de compilation
*   Assurer la fiabilité de services essentiels aux opérations de l'entreprise
*   Surveiller le système quotidiennement et répondre immédiatement aux préoccupations des producteurs et chargés de produits
*   Soutenir les clients d'ici et d'ailleurs, notamment dans la gestion de leurs ressources cryptographiques sensibles

Projets:
    *   Fédération des infrastructures d'hébergement de ressources multimédia

        *   Un total de 3 téraoctets de resources multimédia unifiées sans la moindre minute d'interruption de service.

    *   Parc de compilation de jeux

        *   Recyclage de 50 ordinateurs (Mac, Windows, Linux) dans un parce de compilation automatiquement configuré.
        *   Gestion des trousseaux de compilation pour 12 plateformes-cibles incluant iOS et Android.

    *   Optimisation des temps de compilation

        *   Amélioration observée de 25 heures à moins de 45 minutes (97%).

    *   Unification des outils de compilation dans un intergiciel commun

        *   Diminution estimée des coûts de développement d'outils d'intégration de 80%.


Méthodes:
    BlueGreenDeployment_,
    POO_,
    SOA_,
    TDD_,
    `CI/CD`_,
    Scrum_,
    Kanban_,
    IaC_,
    DocAsCode

Environnement:
    Linux (Debian), Windows 7, Windows 10, C\#, C++, Python, Java, Salt
    Batch, Powershell, Bash, Vim, Jenkins, JFrog Artifactory, Git, Subversion,
    Perforce, Unity 3D, Unreal, Xcode, Android, iOS, GitLab, Active Directory,
    Docker, Nginx, Apache, Visual Studio


Programmeur-analyste
--------------------

*2013 - 2015*, Idéal Technologie

    Programmeur enthousiaste dans un écosystème riche allant de réseaux sociaux
    sur mesure jusqu'à l'achat en ligne en passant par l'automatisation de
    campagnes de marketing.

*   Développer l'API web centrale
*   Rédiger la documentation technique
*   Réaliser l'analyse fonctionnelle de l'API
*   Réaliser l'analyste fonctionnelle des besoins de sécurité de l'application
*   Effectuer le découpage organique de l'API utilisant un modèle d'architecture orientée services (SOA)
*   Fournir un support constant aux programmeurs utilisateurs de l'API
*   Assurer la rétrocompatibilité avec les fonctions préexistantes
*   Modéliser les données des utilisateurs
*   Programmer les nouvelles composantes de sécurité
*   Former et accompagner les programmeurs juniors

Projets:
    *   Refonte des processus d'authentification

        *   Refonte de la modélisation des utilisateurs et des processus d'authentification.

    *   Refonte de l'API web centralisée

        *   Interface d'authentification centralisée (OAuth2);
        *   Traitement d'images sur mesure des produits Ideal Protein;
        *   Géolocalisation des centres affiliés du monde entier.

Méthodes:
    POO_,
    TDD_,
    REST_,
    Scrum_,

Environnement:
    Linux (Debian), Mac OS X, PHP, Javascript, PostgreSQL, Symfony, PHUnit,
    Vagrant, VirtualBox, Git, Bash, JSON, Vim, OAuth2, Varnish, Nginx


Programmeur
-----------

*2012 - 2013*, Précursoft

    Programmeur de logiciels de gestion et d'outils de vente de voiture,
    d'assurance et de services financiers.

*   Développer l'application de vente de voitures Maestro F\&I
*   Programmer différents rapports complexes avec l'API .NET de Crystal Reports
*   Automatiser le déploiement de bases de données du domaine de l'automobile
*   Assurer la rétrocompatibilité avec l'application patrimoniale héritée
*   Rédiger des requêtes SQL performantes
*   Participer aux cérémonies Agiles (Scrum)

Projets:
    Automatisation des déploiements sur les installations privées de concessionnaires.

Méthodes:
    Scrum_,
    POO_,

Environnement:
    Windows 7, Visual Studio 2010, C\# .NET, Microsoft SQL Server, MySQL,
    Cristal Reports, C++ Borland 5, C++ Borland 6, Microsoft SourceSafe,
    XML.


Analyste en sécurité informatique
---------------------------------

*2011 - 2012*, `Revenu Québec <https://www.revenuquebec.ca/fr/>`__

    Stagiaire puis analyste débrouillard impliqué dans la prévention et la
    défense active des réseaux de Revenu Québec.

*   Administrer des serveurs de surveillance réseau (NIDS)
*   Mettre à l'essai différentes solutions logicielles de surveillance réseau
*   Analyser les d'alertes réseau
*   Établir une stratégie d'inspection sans contact du parc informatique
*   Intervenir en cas de tentative d'intrusion réseau
*   Prévenir les tentatives d'intrusion réseau
*   Rédiger des rapports d'enquête
*   Accomplir des audits de sécurité informatique
*   Développer une interface Web
*   Déployer l'application sur des serveurs à haut niveau de sécurité
*   Former et fournir du support technique aux utilisateurs de l'application

Projets:
    *   Surveillance et détection réseau.

        *   Appuyer Revenu Québec dans ces démarches pour assurer la
            disponibilité de tous ses services informatiques ainsi que la
            confidentialité et l’intégrité des données confidentielles qu'elle
            détient.

    *   Interface web de surveillance réseau.

        *   Programmer une interface web simple mais modulaire permettant
            aux différents intervenants impliqués dans les enquêtes réseaux
            d'accéder à l'historique des alertes et d'ajouter dynamiquement
            des outils à l'interface.

Environnement:
    Sourcefire Snort NIDS, TELUS Managed IPS, MySQL, Perl, IIS, Apache,
    Linux (Debian), Linux (Arch Linux), FreeBSD, Windows XP, Visual Basic,
    Ruby, Bash, PHP, Perl, Apache, Visual Basic, VMWare ESXi.


Bénévole, vice-président, animateur, Chef
-----------------------------------------

*2003 - 2016*, Les Amusements Lacrypte inc.

    Leader inspirant d'une bande d'artistes bénévoles fantastiques.

*   Animer des activités thématiques pour jeunes et moins jeunes en plein-air
*   Coordonner tous les responsables et bénévoles lors des activités
*   Assister les scénaristes dans l'élaboration de trames narratives captivantes
*   Présider le comité exécutif en l'absence du président
*   Diriger la cuisine lors de banquets de plus de 50 personnes souvent sans eau courante ni électricité.


.. _SAFe: https://fr.wikipedia.org/wiki/Scaled_agile_framework
.. _Scrum: https://fr.wikipedia.org/wiki/Scrum_(développement)
.. _IaC: https://en.wikipedia.org/wiki/Infrastructure_as_Code
.. _Lean Six Sigma: https://en.wikipedia.org/wiki/Lean_Six_Sigma
.. _REST: http://fr.wikipedia.org/wiki/Representational_State_Transfer
.. _TDD: http://fr.wikipedia.org/wiki/Test_Driven_Development
.. _CI/CD: https://fr.wikipedia.org/wiki/Intégration_continue
.. _Nexus: https://www.scrum.org/resources/online-nexus-guide
.. _Kanban: https://fr.wikipedia.org/wiki/Kanban_(développement)
.. _BlueGreenDeployment: https://martinfowler.com/bliki/BlueGreenDeployment.html
.. _SOA: https://en.wikipedia.org/wiki/Service-oriented_architecture
.. _POO: https://fr.wikipedia.org/wiki/Programmation_orientée_objet
